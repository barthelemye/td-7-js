let objectByName = new Map();

function applyAndRegister(effect){
  effect();
}

function reactive(passiveObject, name){
  objectByName.set(name, passiveObject);
  return passiveObject;
}

function startReactiveDom(){
  for (let elementClickable of document.querySelectorAll("[data-onclick]")){
    const [nomObjet, methode, argument] = elementClickable.dataset.onclick.split(/[.()]+/);
    elementClickable.addEventListener('click', (event) => {
      const objet = objectByName.get(nomObjet);
      objet[methode](argument);
    })
  }
}

